///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 08a - Cat Wrangler
///
/// @file queue.hpp
/// @version 1.0
///
/// A special case of a doubly-linked list.
///
/// @author Marie Wong <marie4@hawaii.edu>
/// @brief  Lab 08a - Cat Wrangler - EE 205 - Spr 2021
/// @date 13_APR_2021 
///////////////////////////////////////////////////////////////////////////////

#pragma once
#include "node.hpp"
#include "list.hpp"

class Queue {
protected:
   DoubleLinkedList list = DoubleLinkedList();

public:
	inline const bool empty() const { return list.empty(); };
	inline unsigned int size () const { return list.size(); };
	inline void push_front( Node* newNode ) { list.push_front( newNode ); };
	inline void pop_back() { list.pop_back(); };
	inline Node* get_first() const { return list.get_first(); };
	inline Node* get_last() const { return list.get_last(); };
	inline Node* get_next( const Node* currentNode ) const { return list.get_next( currentNode ); };

}; // class Queue
